// CRUD Operations
/*
	C - Create (Insert document)
	R - Read/Retrieve (View specific document)
	U - Update (Edit specific document)
	D - Delete (Remove specific document)

	- CRUD Operations are the heart of any backend application.
*/

// [SECTION] Insert a documents (Create)

/*
	- Syntax:
		- db.collectionName.insertOne({object});
	Comparison with javascript
		object.object.method({object});
		arrayName.forEach(variable)

*/
// To Insert One
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "87654321",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS","JavaScript","Phyton"],
	department: "none"
});

/*
Mini Activity

   Scenario: We will create a database that will simulate a hotel database.
   1. Create a new database called "hotel".
   2. Insert a single room in the "rooms" collection with the following details:
                
   name - single
   accommodates - 2
   price - 1000
   description - A simple room with basic necessities
   rooms_available - 10
   isAvailable - false

   3. Use the "db.getCollection('users').find({})" query to check if the document is created.
            
   4. Take a screenshot of the Robo3t result and send it to the batch hangouts.
*/


db.rooms.insertOne({
	name: "single",
    accommodates: "2",
    price: 1000,
    description: "A simple room with basic necessities",
    rooms_available: 10,
    isAvailable: false
});

// Insert Many
/*
	-Syntax:
		- db.collectionName.insertMany([{}])

*/

db.users.insertMany([
{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	},
	courses: ["Phython","React","PHP"],
	department: "none"

},
{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "87654321",
		email: "neilarmstrong@gmail.com"
	},
	courses: ["React","Laravel","Sass"],
	department: "none"

}
]);

/*
1. Using the hotel database, insert multiple room in the "rooms" collection with the following details:

                //Room 1:

                name - double
                accomodates - 3
                price - 2000
                description - A room fit for a small family going on a vacation
                rooms_available - 5
                isAvailable - false

                //Room 2:

                name - queen
                accomodates - 4
                price - 4000
                description - A room with a queen sized bed perfect for a simple getaway
                rooms_available - 15
                isAvailable - false

            2. Use the "db.getCollection('users').find({})" query to check if the document is created.
            
            3. Take a screenshot of the Robo3t result and send it to the batch hangouts.\

*/

db.rooms.insertMany([
{
 	name: "double",
 	accomodates: 3,
    price: 2000,
    description: "A room fit for a small family going on a vacation",
    rooms_available: "5",
    isAvailable: false
},
{
	name: "queen",
	accomodates: 4,
    price: 4000,
    description: "A room with a queen sized bed perfect for a simple getaway",
    rooms_available: 15,
    isAvailable: false	
}
]);

// [SECTION] Retrieve/Read a document (Read)
/*
	Syntax:
		- db.collectionName.find({});// get all the documents
		- db.collectionName.find({field:value}); //get a specific document

*/
// Find all the documents in the collection
db.users.find({});

// Find a specific documents in the collection using the field and a value.
db.users.find({firstName: "Stephen"});
db.users.find({department: "none"});

// Find documents with multiple parameters.
/*
	Syntax:
		- db.collectionName.find({fieldA:valueA}, {fieldB:valueB});
*/

db.users.find({lastName: "Armstrong", age: 82});

// [SECTION] Updating documents (Update)
// Create a document to update

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00000000",
		email: "test@gmail.com"
	},
	courses:[],
	department: "none"
});

/*
	- Just like the find method, methods that only manipulate a single document will only update the FIRST document that matches the search criteria.
	Syntax:
		- db.collectionName.updateOne({criteria},{$set: {field:value}});

*/

db.users.updateOne(
	{firstName: "Test"},
	{
		$set:{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@gmail.com"
			},
			course: ["PHP","Laravel","HTML"],
			department: "Operations",
			status: "active"
		}
	}
);


// db.users.updateOne(
// 	{_id: ObjectId("63081ef70d5a8516253eacce")},
// 	{
// 		$set:{department: "Operations"}
// 	}

// );


// Updating multiple documents
/*
	- Syntax:
		db.collectionName.updateMany({criteria},{$set: {field:value}});
*/

db.users.updateMany(
	{"department" : "none"},
	{
		$set:{department: "HR"}
	}
);

// Replace One
/*
	- Can be used if replacing the whole document if necessary.
	Syntax:
		db.collectionName.replaceOnce({criteria}, {field: value});

*/
db.users.replaceOne(
	{firstName: "Bill"},
	{
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@gmail.com"
		},
		courses: ["PHP","Laravel","HTML"],
		department: "Operations"
	}

);

/*
    Mini Activity:

        1. Using the hotel database, update the queen room and set the available rooms to zero.

        2. Use the find query to validate if the room is successfully updated.

        3. Take a screenshot of the Robo3t result and send it to the batch hangouts.

*/

db.rooms.updateOne(
	{name: "queen"},
	{
		$set:{rooms_available: 0}
	}
);

// [SECTION] Removing documents [DELETE]

// Deleting a single document
/*
	Syntax:
		db.collectionName.deleteOne({criteria}); //deletes the first document found

*/

	db.users.deleteOne({
		firstName: "Test"
	});

// Delete Many
	/*
		- Be careful when using "deleteMany" method. If no search criteria is provided, it will delete all documents in the collection.

		Syntax:
			db.collectionName.deleteMany({criteria});
			db.collectionName.deleteMany({}); //DO NOT USE

	*/

	db.users.deleteMany({
		firstName: "Test"
	});

	/*

    Mini Activity:

    1. Using the hotel database, delete all rooms with 0 available rooms.

    2. Use the find query to show all the rooms and check if the 0 available room is deleted.

    3. Take a screenshot of the Robo3t result and send it to the batch hangouts.

*/

db.rooms.deleteMany({
	rooms_available: 0
});

// [SECTION] Advance queries
/*
	

*/

// Query an embedded document

db.users.find({
	contact:{
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
});

// Query on nested field
// dot notation also works when accessing a nested field
db.users.find({
	"contact.email": "stephenhawking@gmail.com"
});

// Querying an array with exact element
db.users.find(
	{
		courses: ["PHP", "Laravel", "HTML"]
	}
);

// Querying an array disregarding the array elements order.
// %all matches documents where the field contains the nested array elements.
db.users.find(
	{
		courses: {$all: ["JavaScript", "CSS", "Phython"]}
	}
);

// will retrieve two documents containing the "phython" as element of the nested array.
db.users.find(
	{
		courses: {$all: ["Phython"]}
	}
);

// OR

db.users.find(
    {
    courses: /Phython/
}
);



// Querying an embedded array
db.users.insertOne({
	nameArr:[
		{
			nameA: "Juan"
		},
		{
			nameB: "Tamad"
		}
	]
});

// for finding
db.users.find({
	nameArr:{
		nameA: "Juan"
	}
})